package com.dynamodb.demo.repository;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBSaveExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue;
import com.dynamodb.demo.entity.Employee;

import ch.qos.logback.classic.Logger;

@Repository
public class EmployeeRepo {
	
	@Autowired
	private DynamoDBMapper dynamoDBMapper;
	
	
	public Employee save(Employee employee) {
		dynamoDBMapper.save(employee);
		return employee;
	}
	
	public Employee getEmployeeById(String employeeId) {
		return dynamoDBMapper.load(Employee.class, employeeId);
	}
	
	public String delete(String employeeId) {
		Employee emp= dynamoDBMapper.load(Employee.class, employeeId);
				dynamoDBMapper.delete(emp);
		return "Employee details deleted";
		
	}
	
	public String updateEmployee(Employee employee) {
	
			dynamoDBMapper.save(employee, buildDynamodbSaveExpression(employee));
			return "Details updated";
		
	}
	
	public DynamoDBSaveExpression buildDynamodbSaveExpression(Employee employee) {
		DynamoDBSaveExpression saveExpression = new DynamoDBSaveExpression();
		Map<String, ExpectedAttributeValue> expected = new HashMap<>();
		expected.put("employeeid",new ExpectedAttributeValue(new AttributeValue(employee.getEmployeeid()))
				.withComparisonOperator(ComparisonOperator.EQ));
		saveExpression.setExpected(expected);
		return saveExpression;
	}
	
	

}
